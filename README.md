# Sentdex Discord bots with Discordpy Tutorial

## Packages
- discord.py-rewrite

## Installing discord.py-rewrite
To get the package, do one of the following:
- Go to https://github.com/Rapptz/discord.py/tree/rewrite and download the ZIP
- `git clone https://github.com/Rapptz/discord.py.git`

To install the package:
1. `pip install -r requirements.txt`
2. `python setup.py install`

## Usage
- Create `guild.txt` and `token.txt` and put the guild id and token in the respective `.txt` files.
- `python discord-tutorial-video.py`